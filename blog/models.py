from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categories'


class Post(models.Model):
    TYPE_NOTE = 0
    TYPE_ARTICLE = 1

    title = models.CharField(max_length=255)
    content = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)
    mod_date = models.DateTimeField(auto_now=True)
    category = models.ManyToManyField(Category)

    def __unicode__(self):
        return self.title