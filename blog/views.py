from blog.models import Post, Category
from django.shortcuts import render, get_list_or_404, get_object_or_404, render_to_response


def index(request):
    posts = Post.objects.all().order_by('pub_date')
    categories = get_list_or_404(Category)
    return render(request, 'blog/post.html', {'post': posts[0], 'posts': posts, 'categories': categories})


def archive(request, category_id=None):
    categories = get_list_or_404(Category)
    if (category_id):
        category = Category.objects.get(id=category_id)
        posts = category.post_set.all()
    else:
        posts = Post.objects.all().order_by('pub_date')

    return render(request, 'blog/archive.html', {'posts': posts, 'categories': categories})


def view(request, id):
    categories = get_list_or_404(Category)
    post = get_object_or_404(Post, id=id)
    return render(request, 'blog/post.html', {'post': post, 'categories': categories})
