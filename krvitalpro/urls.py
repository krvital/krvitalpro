from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'blog.views.index'),
    url(r'^archive/$', 'blog.views.archive',),
    url(r'^archive/(?P<category_id>\d)/$', 'blog.views.archive',),
    url(r'^post/(\d+)$', 'blog.views.view'),
    url(r'^portfolio/$', TemplateView.as_view(template_name='common/portfolio.html')),
    url(r'^about/$', TemplateView.as_view(template_name='common/about.html')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
