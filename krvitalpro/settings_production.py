from settings import *


STATIC_ROOT = '/home/krvital/sites/krvitalpro/static/'
STATIC_URL = 'http://krvital.pro/static/'

TEMPLATE_DIRS = (
    '/home/krvital/sites/krvitalpro/templates/'
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)
